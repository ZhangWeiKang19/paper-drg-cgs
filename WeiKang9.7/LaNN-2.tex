%!TEX root = ./LaNN-0.tex

\section{Approach}
\label{Approach}
Fig.~\ref{figure1} presents an overview of the proposed length-adaptive neural network for answer selection.
The model consists of three main components, i.e., word representation (see \S\ref{Word Representation}), feature extractor (see \S\ref{Feature Extractor}), and sentence embedding and answer ranking (see \S\ref{Sentence embedding and answer ranking}).
%
\begin{figure}[t]
%\vspace*{-0.75\baselineskip}
        \centering
        \includegraphics[width=0.75\columnwidth]{Figures/frame}
\caption{Overview of the length-adaptive neural network for answer selection.}
\label{figure1}
\end{figure}
%

\subsection{Word representation}
\label{Word Representation}
Let $s$ be an input sentence (question or answer) with length $L$.
To keep as many word features as possible, we produce the word representation corresponding to each word $w_t$ in $s$ not only from frozen pre-trained word embeddings but also fine-tuning word embeddings in the training process. 
Then, we concatenate these two word embeddings to form a combined word vector. 
Finally, we deploy a hidden layer to select useful features from the concatenated word vector.
The final representation $r_{w_t}$ {(of dimension $D$)} of the word $w_t$ is calculated according to:
%
\begin{equation}
\label{equation1}
{r_{w_t}}=\tanh({W_h}\cdot({r_{w_t}^{fr}} \oplus {r_{w_t}^{ft}}) + {b_h}),
\end{equation}
%
where $r_{w_t}^{fr}$ and $r_{w_t}^{ft}$ are corresponding word vectors from the frozen word embeddings and the fine-tuning word embeddings, respectively; $W_h\in {\mathbb{R}^{D \times D}}$ and $b_h\in {\mathbb{R}^{D \times 1}}$ are network parameters of the hidden layer.
Together, the word representations of $s$ form the word representation matrix:
%
\begin{equation}
\label{equation2}
R_W = \left( 
\begin{array}{cccc}
{r_{w_1}} & {r_{w_2}} & \cdots & {r_{w_L}}
\end{array}
\right).
\end{equation}
%

\subsection{Feature extractor}
\label{Feature Extractor}
Next, we design a length-adaptive neural network as the feature extractor, which aims at making good use of the context information of words in $s$ to generate a high-quality sentence representation for $s$.
Most previous neural networks for answer selection \citep[e.g.,][]{Feng2016Applying, tan2016improved}  do not distinguish between input sentences of different lengths.
In contrast, we deploy a BiLSTM-based and a Transformer-based feature extractor to deal with sentences of different lengths for generating sentence embeddings.
We employ two flags, i.e., ${\emph{flag}}_{\rm{lstm}}$ and ${\emph{flag}}_{\rm{tsfm}}$, for each feature extractor according to the sentence length $L$ as follows:
%
\begin{equation}
\label{equation3}
\left\{ \begin{array}{l}
{\emph{flag}}_{{\rm{lstm}}}{\rm{ = 1}} \text{~and~} {\emph{flag}}_{{\rm{tsfm}}}{\rm{ = 0}}, ({{L}} < {{L}}_{\rm{threshold}})\\
{\emph{flag}}_{{\rm{lstm}}}{\rm{ = 0}} \text{~and~}  {\emph{flag}}_{{\rm{tsfm}}}{\rm{ = 1}}, ({{L}} \geq {{L}}_{\rm{threshold}}),
\end{array} \right.
\end{equation}
%
where ${{L}}_{\rm{threshold}}$ is a preset threshold to judge whether the input sentence $s$ is long or not. 
The flags will be employed to activate the corresponding feature extractor by multiplying the input word representation matrix $R_W$ with the value of {flags} as follows:
%
\begin{align}
\label{equation4-5}
R_W^{{\rm{lstm}}} &= {R_W} \cdot {\emph{flag}}_{\rm{lstm}} \\
R_W^{{\rm{tsfm}}} &= {R_W} \cdot {\emph{flag}}_{\rm{tsfm}},
\end{align}
%
where $R_W^{{\rm{lstm}}}$ and $R_W^{{\rm{tsfm}}}$ are updated input representation matrixes for the BiLSTM-based and Transformer-based feature extractors, respectively.

For a short sentence, we activate the BiLSTM-based feature extractor by setting ${\emph{flag}}_{\rm{lstm}}$ to $1$ and ${\emph{flag}}_{\rm{tsfm}}$ to $0$, which leads to a null representation matrix in the Transformer-based feature extractor.
Detailed operations on the $t$-th word representation $r_{w_t}^{{\rm{lstm}}}$ in matrix $R_W^{{\rm{lstm}}} $ are:
%
\begin{equation}
\label{equation6}
\left\{ 
\begin{array}{rcl}
{f_t} &=& \sigma ({W_f}\cdot{r_{w_t}^{{\rm{lstm}}}}{\rm{ + }}{U_f}\cdot{h_{t - 1}} + {b_f}) \\
{i_t} &=& \sigma ({W_i}\cdot{r_{w_t}^{{\rm{lstm}}}}{\rm{ + }}{U_i}\cdot{h_{t - 1}} + {b_i}) \\
\widetilde {{C_t}} &=& \tanh ({W_c}\cdot{r_{w_t}^{{\rm{lstm}}}}{\rm{ + }}{U_c}\cdot{h_{t - 1}} + {b_c}) \\
{C_t} &=& {f_t} \cdot {C_{t - 1}} + {i_t} \cdot \widetilde {{C_t}} \\
{o_t} &=& \sigma ({W_o}\cdot{r_{w_t}^{{\rm{lstm}}}}{\rm{ + }}{U_o}\cdot{h_{t - 1}} + {b_o}) \\
{h_t} &=& {o_t} \cdot \tanh ({C_t}),
\end{array} 
\right.
\end{equation}
%
where $i$, $f$ and $o$ represent the input gate, the forget gate and the output gate, respectively; {$h$ represents the memorized word representation (of dimension $H$); $\widetilde {{C}}$ and ${C}$ are the overall and the present memory; $\sigma$ is the activation function $\text{sigmoid}$; $W \in {\mathbb{R}^{H \times D}}$, $U \in {\mathbb{R}^{H \times H}}$ and $b \in {\mathbb{R}^{H \times 1}}$ are the network parameters, determining the input information, output information and bias, respectively.}
After deploying an LSTM in two directions, we obtain a BiLSTM-based sentence representation matrix $R_s^{L\!S} $ as follows:
%
\begin{align}
\label{equation7-8}
r_{w_t}^{L\!S} &= \overrightarrow {{h_t}} || {\overleftarrow {{h_t}}},\\
R_s^{L\!S} &= 
\left(
\begin{array}{cccc}
r_{w_1}^{L\!S} & r_{w_2}^{L\!S} & \cdots & r_{w_L}^{L\!S}
\end{array}
\right),
\end{align}
%
where $||$ indicates concatenation of two vectors.

For a long sentence, we activate the Transformer-based feature extractor by setting  ${\emph{flag}}_{\rm{lstm}}$ to $0$ and ${\emph{flag}}_{\rm{tsfm}}$ to $1$.
%
%Since the Transformer-based feature extractor contains no recurrence and convolution,
Following \citep{Vaswani2017Attention}, we employ a positional encoding to inject sequential information and generate an
%into the input matrix $R_W^{{\rm{tsfm}}}$, leading to an
updated $R_W^{{\rm{tsfm}}}$.
%
{After that,
%Unlike the scaled Dot-Product function in the original Transformer, we employ
a scaled perception function is applied to calculate self-attentive similarity in $R_W^{{\rm{tsfm}}}$:
%
%. A self-attentive word vector ${{r_{w_t}}^a}$ will be generated as follows:
%
%\begin{align}
%\label{equation9-10}
%f(r_{w_t}^{{\rm{tsfm}}},R_W^{{\rm{tsfm}}}) &= o_a^T \cdot tanh ({W_a} \cdot r_{w_t}^{{\rm{tsfm}}} + {U_a} \cdot R_W^{{\rm{tsfm}}}),\\
%{{r_{w_t}}^a} &= softmax (\frac{f(r_{w_t}^{{\rm{tsfm}}},R_W^{{\rm{tsfm}}})}{\sqrt {d_{R_W^{{\rm{tsfm}}}}}})\cdot R_W^{{\rm{tsfm}}},
%\end{align}
%%
%
\begin{equation}
\label{equation10}
f(R_W^{{\rm{tsfm}}},R_W^{{\rm{tsfm}}}) = O_a^{\rm{T}} \cdot \tanh ({W_a} \cdot R_W^{{\rm{tsfm}}} + {U_a} \cdot R_W^{{\rm{tsfm}}}),
\end{equation}
%
where ${O_a}\in {\mathbb{R}^{D \times L}}$, ${W_a}\in {\mathbb{R}^{D \times D}}$ and ${U_a}\in {\mathbb{R}^{D \times D}}$ are the attention parameters. Then, the self-attentive sentence representation matrix $R_s^a$ is produced by:
%
\begin{align}
\label{equation10-11}
R_s^a &= R_W^{{\rm{tsfm}}} \cdot
 \text{softmax}
 \left(\frac{f(R_W^{{\rm{tsfm}}},R_W^{{\rm{tsfm}}})}{\sqrt {d_{R_W^{{\rm{tsfm}}}}}}^{\rm{T}}\right)\\
&= 
\left( 
\begin{array}{cccc}
{r_{w_1}^a} & {r_{w_2}^a} & \cdots & {r_{w_L}^a}
\end{array}
\right).
\nonumber
\end{align}
%
where ${d_{R_W^{\rm{tsfm}}}}$ is the dimension of the row vector in $R_W^{{\rm{tsfm}}}$; ${\sqrt {d_{R_W^{{\rm{tsfm}}}}}}$ aims to scale the $\text{softmax}$ function to regulate its value.
Finally, ${r_{w_t}^a}$ is the $t$-th self-attentive word vector in $R_s^a$.}

We then adopt a multi-head mechanism \citep{Vaswani2017Attention} to jointly incorporate information from different representation subspaces.
Assuming that there are $n$ heads in the Transformer-based feature extractor, the $n$ self-attentive sentence representation matrices will be concatenated to form the output sentence representation matrix $R_s^{T\!F}$ as:
\begin{equation}
\begin{aligned}
\label{equation12}
R_s^{T\!F} &= R_s^{a_1} \oplus R_s^{a_2} \oplus \cdots \oplus R_s^{a_n}\\
  &= 
\left(
\begin{array}{cccc}
 {r_{w_1}^{T\!F}} & {r_{w_2}^{T\!F}} & \cdots & {r_{w_l}^{T\!F}} 
\end{array}
\right),
\end{aligned}
\end{equation}
%
where $R_s^{a_i}$ is the $i$-th self-attentive sentence representation matrix.

\subsection{Sentence embedding and answer ranking}
\label{Sentence embedding and answer ranking}
After generating the sentence representation matrix in \S\ref{Feature Extractor}, we employ an attentive pooling to generate the sentence embeddings $v_q$ and $v_a$ for question and answer from their corresponding sentence representation matrixes $R_Q$ and $R_A$ according to:
\begin{align}
\label{equation13-15}
G &= {R_Q}^{\rm{T}} \cdot U \cdot {R_A},\\
v_q &= {R_Q} \cdot \text{softmax} (ColumnMax(G)),\\
v_a &= {R_A} \cdot \text{softmax} (RowMax(G)),
\end{align}
where $G$ are the attentive similarities between $R_Q$ and $R_A$; $U\in {\mathbb{R}^{D \times D}}$ is the attention parameter; $ColumnMax(\cdot)$ (or $RowMax(\cdot)$) is a function that returns the max value of a column (or row) vector of a matrix.
%
%and $RowMax$ are functions that returns the max value of a column vector and a row vector of a matrix, respectively.
Following \citep{tan2016improved, santos2016attentive, wang2016inner}, we compute the relevance between question and answer by measuring the cosine similarity of their sentence embeddings.

In the training phase, each training instance consists of a question $q$, a positive answer $a^+$ (a ground truth) and a negative answer $a^-$ (an incorrect answer) randomly sampled from all answers in the training set.
We train the neural network for the best training epoch by minimizing the following ranking loss of candidate answers:
%. In this paper, we adopt the same ranking loss function in \citep{Feng2016Applying} as follows:
%
\begin{equation}
\label{equation22}
loss = \max \{0, m - \cos(v_q, v_{a^+}) + \cos(v_q, v_{a^-})\},
\end{equation}
%
where $m$ is a preset margin to judge if a training instance will be terminated or not. By doing so, we can rank the candidate answers for each question according to their relevance towards the corresponding question.
